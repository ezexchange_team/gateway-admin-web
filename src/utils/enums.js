const EnumRoleType = {
  ADMIN: 'admin',
  DEFAULT: 'gateway',
  DEVELOPER: 'user',
}

module.exports = {
  EnumRoleType,
}
