const APIV1 = '/transfer-express-services/api'
const APIV12 = '/transfer-express-services/api/v1.2'

module.exports = {
  name: '亚马逊收款管理',
  prefix: 'easypayxAdmin',
  footerText: 'EasyPay Design Admin  © 2017',
  logo: '/logo.png',
  iconFontCSS: '/iconfont.css',
  iconFontJS: '/iconfont.js',
  CORS: [],
  openPages: ['/login'],
  apiPrefix: '/api',
  APIV1,
  api: {
    userLogin: '/transfer-express-services/login',
    userLogout: `${APIV1}/user/logout`,
    userInfo1: `${APIV1}/userInfo`,
    users: `${APIV1}/admin/users`,
    user: `${APIV1}/users/:id/view`,
    userLegalEntities: `${APIV1}/admin/users/:id/entities`,
    userAmazonEntities: `${APIV1}/admin/users/:id/amazon-legal-entities`,
    userInfo: `${APIV1}/users/:id/view`,
    userDetail: `${APIV1}/aiyou/users/:id/detail`,
    userAuths: `${APIV12}/admin/certifications`,
    assignVirtualBank: `${APIV1}/admin/virtual-accounts/:id`,
    rejectVirtualBank: `${APIV1}/admin/amazon-legal-entity/:id/rejected`,
    v1test: `${APIV1}/test`,
    passUserCertification: `${APIV1}/admin/legal-entity/:entityId/status/passed`,
    rejectCertification: `${APIV1}/admin/legal-entity/:entityId/status/rejected`,
    virtualAccountApplicatijons: `${APIV12}/admin/virtual-accounts`,
    amazonFinancialEvents: `${APIV1}/admin/financial-event-groups`,

    updateUserFeeRate: `${APIV1}/admin/user-fee-rates`,
  },
}
