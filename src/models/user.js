/* global window */
import modelExtend from 'dva-model-extend'
import pathToRegexp from 'path-to-regexp'
import { fetchUserAmazonEntities, fetchUserDetail, fetchUserLegalEntities } from '../services/user'
import { assignVirtualBankAccount } from '../services/bank'
import { pageModel } from './common'

export default modelExtend(pageModel, {
  namespace: 'user',

  state: {
    userLegalEntities: [],
    userInfo: {},
    userAmazonEntities: [],
    assignBankAccountVisible: false,
    assignBankAccountData: {},
    assignBankAccountLoading: false,
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        console.log(location)
        const match = pathToRegexp('/user/:id').exec(location.pathname)
        if (match) {
          dispatch({
            type: 'fetchUserLegalEntites',
            payload: { userId: match[1] },
          })

          dispatch({
            type: 'fetchUserDetail',
            payload: { id: match[1] },
          })

          dispatch({
            type: 'fetchUserAmazonEntities',
            payload: { userId: match[1] },
          })
        }
      })
    },
  },

  effects: {
    * fetchUserDetail({ payload }, { call, put }) {
      const { data } = yield call(fetchUserDetail, payload);
      yield put({ type: 'save', payload: { userInfo: data } })
    },

    * fetchUserLegalEntites({ payload }, { call, put }) {
      const { data } = yield call(fetchUserLegalEntities, payload);
      yield put({ type: 'save', payload: { userLegalEntities: data.list } })
    },

    * fetchUserAmazonEntities({ payload }, { call, put }) {
      const { data } = yield call(fetchUserAmazonEntities, payload);
      console.log(data)
      yield put({ type: 'save', payload: { userAmazonEntities: data.list } })
    },


    * showAssignVirtualAccountForm({ payload }, { put }) {
      yield put({ type: 'save', payload: { assignBankAccountVisible: true, assignBankAccountData: payload } })
    },

    * closeAssignVirtualAccountForm({ payload }, { put }) {
      yield put({ type: 'save', payload: { assignBankAccountVisible: false, assignBankAccountData: payload } })
    },

    * assignVirtualBankAccount({ payload }, { call, put }) {
      yield put({ type: 'save', payload: { assignBankAccountLoading: true } })
      try {
        yield call(assignVirtualBankAccount, payload);
      } catch (e) {
        yield put({ type: 'save', payload: { assignBankAccountLoading: false } })
        throw e;
      }
      yield put({ type: 'save', payload: { assignBankAccountLoading: false } })
      yield put({ type: 'save', payload: { assignBankAccountVisible: false, assignBankAccountData: payload } })
      yield put({ type: 'fetchUserAmazonEntities' })

    },
  },

  reducers: {
    save(state, { payload }) {
      return Object.assign({}, state, payload)
    },
  },
})
