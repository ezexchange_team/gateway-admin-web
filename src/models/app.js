/* global window */
/* global document */
/* global location */
import { routerRedux } from 'dva/router';
import config from 'config';
import { EnumRoleType } from 'enums';
import { logout, query } from 'services/app';
import { fetchGatewayUserInfo } from 'services/users'
import queryString from 'query-string';

const { prefix } = config

export default {
  namespace: 'app',
  state: {
    user: {},
    permissions: {
      visit: [],
    },
    menu: [
      {
        id: 1,
        icon: 'laptop',
        name: '首页',
        route: '/dashboard',
      }, {
        id: 2,
        icon: 'laptop',
        name: '用户管理',
        route: '/users',
      }, {
        id: 3,
        icon: 'laptop',
        name: '网关',
        route: '/gateways',
      },
    ],
    menuPopoverVisible: false,
    siderFold: window.localStorage.getItem(`${prefix}siderFold`) === 'true',
    darkTheme: true,
    isNavbar: document.body.clientWidth < 769,
    navOpenKeys: JSON.parse(window.localStorage.getItem(`${prefix}navOpenKeys`)) || [],
    locationPathname: '',
    locationQuery: {},
  },
  subscriptions: {
    setupHistory ({ dispatch, history }) {
      history.listen((location) => {
        const auth = sessionStorage.getItem("auth");
        if (!auth) {
          if(location.pathname !== '/login') {
            dispatch(routerRedux.push({
              pathname: '/login',
              query: {
                from: location.pathname,
              },
            }))
          }
        }
        dispatch({
          type: 'updateState',
          payload: {
            locationPathname: location.pathname,
            locationQuery: queryString.parse(location.search),
          },
        })
      })
    },

    setup ({ dispatch }) {
      dispatch({ type: 'fetchGatewayUserInfo' })
      let tid
      window.onresize = () => {
        clearTimeout(tid)
        tid = setTimeout(() => {
          dispatch({ type: 'changeNavbar' })
        }, 300)
      }
    },

  },
  effects: {
    * fetchGatewayUserInfo ({ payload }, { call, put, select }) {
      try {
        const { data, headers } = yield call(fetchGatewayUserInfo, payload)

      } catch (e) {

      }
    },

    * logout ({ payload }, { call, put }) {
      // const data = yield call(logout, parse(payload))
      sessionStorage.setItem('auth', '');
      yield put(routerRedux.push({
        pathname: '/login',
        query: {
          from: 'user',
        },
      }))
    },

    * changeNavbar (action, { put, select }) {
      const { app } = yield (select(_ => _))
      const isNavbar = document.body.clientWidth < 769
      if (isNavbar !== app.isNavbar) {
        yield put({ type: 'handleNavbar', payload: isNavbar })
      }
    },

  },
  reducers: {
    updateState (state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },

    switchSider (state) {
      window.localStorage.setItem(`${prefix}siderFold`, !state.siderFold)
      return {
        ...state,
        siderFold: !state.siderFold,
      }
    },

    switchTheme (state) {
      window.localStorage.setItem(`${prefix}darkTheme`, !state.darkTheme)
      return {
        ...state,
        darkTheme: !state.darkTheme,
      }
    },

    switchMenuPopver (state) {
      return {
        ...state,
        menuPopoverVisible: !state.menuPopoverVisible,
      }
    },

    handleNavbar (state, { payload }) {
      return {
        ...state,
        isNavbar: payload,
      }
    },

    handleNavOpenKeys (state, { payload: navOpenKeys }) {
      return {
        ...state,
        ...navOpenKeys,
      }
    },
  },
}
