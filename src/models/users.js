/* global window */
import modelExtend from 'dva-model-extend'
import { config } from 'utils'
import * as userService from 'services/user'
import { create, remove, update } from 'services/user'
import * as authService from 'services/authentications'
import * as usersService from 'services/users'
import * as bankService from 'services/bank'
import * as amazonService from 'services/amazons'
import queryString from 'query-string'
import { pageModel } from './common'

export default modelExtend(pageModel, {
  namespace: 'users',

  state: {
    users: [],
    auths: [],
    virtualAccounts: [],
    virtualAccountOperation: {
      loading: false,
      visibleOpterationType: -1,
      virtualBankAccount: {},
      extraData: {},
    },
    certificationOptCompleted: true,

  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        if (location.pathname === '/users') {
          dispatch({
            type: 'fetchUserAuths',
            payload: queryString.parse(location.search),
          })

          dispatch({
            type: 'fetchVirtualAccountApplications',
            payload: queryString.parse(location.search),
          })
        }
      })
    },
  },

  effects: {
    * fetchUserAuths({ payload }, { call, put }) {
      const userInfo = JSON.parse(sessionStorage.getItem('userInfo'));
      const { data, header } = yield call(authService.fetchUserAuths, { gatewayId: userInfo.gatewayId, status: '0', current: '0' });
      console.log(data)
      yield put({ type: 'save', payload: { auths: data } })
    },

    * fetchVirtualAccountApplications({ payload }, { call, put }) {
      const userInfo = JSON.parse(sessionStorage.getItem('userInfo'));
      const { data, header } = yield call(usersService.fetchVirtualAccountApplications, {
        gatewayId: userInfo.gatewayId,
        status: '0',
        current: '0'
      });
      yield put({ type: 'save', payload: { virtualAccounts: data } })
    },

    * fetchUsers({ payload }, { call, put }) {
      const { data, header } = yield call(usersService.fetchUsers);
      yield put({ type: 'save', payload: { users: data.list } })
    },

    * approveUserCertifiction({ payload }, { call, put }) {
      console.log(payload)
      yield put({
        type: 'save',
        payload: { certificationOptCompleted: false }
      })
      const { data, header } = yield call(authService.approveUserCertifiction, payload);
      yield put({
        type: 'save',
        payload: { certificationOptCompleted: true }
      })
      yield put({
        type: 'fetchUserAuths'
      })

    },

    * rejectUserCertifiction({ payload }, { call, put }) {
      console.log(payload)
      yield put({
        type: 'save',
        payload: { certificationOptCompleted: false }
      })
      const { data, header } = yield call(authService.rejectUserCertifiction, payload);
      yield put({
        type: 'save',
        payload: { certificationOptCompleted: true }
      })
      yield put({
        type: 'fetchUserAuths'
      })

    },

    * openOperationModal({ payload }, { call, put }) {
      yield put({ type: 'save',
        payload: {
          virtualAccountOperation: {
            loading: false,
            visibleOpterationType: payload.type,
            virtualBankAccount: payload.record,
            extraData: {},
          },
        },
      })
      if(payload.type ===2 ){
        yield put({ type: 'saveVirtualAccountOperation', payload: { loading: true} })
        const { data } = yield call(amazonService.fetchAmazonFinancialEvents,
          {
            storeName:payload.record.legalEntity.entityName,
            sellerId: payload.record.legalEntity.sellerId,
            accessId: payload.record.legalEntity.accessId,
            secretKey: payload.record.legalEntity.secretKey
          })
        yield put({ type: 'saveVirtualAccountOperation', payload: { loading: false,
          extraData: data.listFinancialEventGroupsResult} })
      }
    },

    * closeOperationModal({}, { put }) {
      yield put({ type: 'save',
        payload: {
          virtualAccountOperation: {
            loading: false,
            visibleOpterationType: -1,
            virtualBankAccount: {},
            extraData: {},
          },
        },
      })
    },

    * assignVirtualBankAccount({ payload }, { call, put }) {
      yield put({ type: 'saveVirtualAccountOperation', payload: { loading: true} })
      try {
        yield call(bankService.assignVirtualBankAccount, payload);
      } catch (e) {
        yield put({ type: 'saveVirtualAccountOperation', payload: { loading: false} })
        throw e;
      }

      yield put({ type: 'saveVirtualAccountOperation', payload: { loading: false} })
      yield put({ type: 'closeOperationModal'})
      yield put({ type: 'fetchVirtualAccountApplications' })
    },

    * rejectVirtualBankAccount({ payload }, { call, put }) {
      yield put({ type: 'saveVirtualAccountOperation', payload: { loading: true} })
      try {
        yield call(bankService.rejectVirtualBankAccount, payload);
      } catch (e) {
        yield put({ type: 'saveVirtualAccountOperation', payload: { loading: false} })
        throw e;
      }

      yield put({ type: 'saveVirtualAccountOperation', payload: { loading: false} })
      yield put({ type: 'closeOperationModal'})
      yield put({ type: 'fetchVirtualAccountApplications' })
    },

  },

  reducers: {
    save(state, { payload }) {
      // console.log(payload)
      // console.log(Object.assign({}, state, payload))
      return Object.assign({}, state, payload)
    },

    saveVirtualAccountOperation(state, { payload }) {
      // console.log(payload)
      // console.log(Object.assign({}, state, payload))
      const virtualAccountOperation =  Object.assign({}, state.virtualAccountOperation, payload)
      console.log(virtualAccountOperation)
      return {...state, virtualAccountOperation}
    },

  },
})
