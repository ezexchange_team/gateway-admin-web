import Search from './Search'
import Loader from './Loader'
import DataTable from './DataTable'
import * as Layout from './Layout/index.js'
import Page from './Page'


export {
  Layout,
  Search,
  Loader,
  Page,
  DataTable,
}
