import React from 'react'
import { Modal, Table, Dropdown, Icon, Menu } from 'antd'
import { Link } from 'dva/router'

const UserCertifications = ({ loading, certifications, rejectOpt, approveOpt, actionCompleted }) => {

  const handleMenuClick = (record,item) =>{
    switch (item.key) {
      case 0:
        rejectOpt(record, '实名信息不完整')
        break
      case 1:
        rejectOpt(record, '证件扫描资料无法识别')
        break
      case 2:
        rejectOpt(record, '无法核实实名信息')
        break
      default:
        rejectOpt(record, '无法核实实名信息')
        break
    }
  }

  const columns = [{
    title: '#',
    dataIndex: 'id',
    render: (text, record) => {
      return <Link to={'/user/' + text}>{text}</Link>
    }
  }, {
    title: '实名',
    dataIndex: 'entityName',
  }, {
    title: '实名认证号码',
    dataIndex: 'identityNumber',
  }, {
    title: '注册地',
    dataIndex: 'registrationLocation'
  }, {
    title: '操作',
    dataIndex: "status",
    key:"operation",
    render: (text,record) => {
      if(text === 0){
        return (
          <div >
            <a className="ant-dropdown-link" onClick={approveOpt.bind(this,record)} disabled={!actionCompleted}>通过</a>
            <Dropdown overlay={
              <Menu onClick={handleMenuClick.bind(this,record)}>
                <Menu.Item key="0">实名信息不完整</Menu.Item>
                <Menu.Item key="1">证件扫描资料无法识别</Menu.Item>
                <Menu.Item key="2">无法核实实名信息</Menu.Item>
              </Menu>
            } trigger={['click']} disabled={!actionCompleted} >
              <a className="ant-dropdown-link" href="#" style={{marginLeft:"4px",paddingLeft:"4px",borderLeft:"1px solid"}}>
                拒绝 <Icon type="down" />
              </a>
            </Dropdown>
          </div>

        )
      }
    },
  }
  ]

  return (
    <div>
      <Modal visible={false}>fdfd</Modal>
      <Table
        columns={columns}
        dataSource={certifications}
        loading={loading}

      ></Table></div>
  )
}

export default UserCertifications;
