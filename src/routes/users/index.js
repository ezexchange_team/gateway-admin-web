import React from 'react';
import { Link } from 'dva/router'
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { Avatar, Card, Col, Dropdown, Icon, Menu, Row, Table } from 'antd';
import { config } from 'utils';
import UserCertifications from '../commons/UserCertifications'
import VirtualAccounts from '../commons/VirtualAccounts';


const Users = ({ loading, dispatch, users }) => {

  console.log(users)

  const columns = [{
    title: "#",
    dataIndex: "id",
    render: (text,record) =>{
      return <Link to={"/user/"+text}>{text}</Link>
    }
  }, {
    title: "用户名",
    dataIndex: "username",
  }, {
    title: "邮箱",
    dataIndex: "email",
  }, {
    title: "网关",
    dataIndex: "gatewayId"
  }]



  // const approveUserLegal = (record) =>{
  //   console.log(record);
  //   dispatch({
  //     type: "users/approveUserCertifiction",
  //     payload:{entityId:record.id}
  //   })
  // }
  //
  // const rejectUserLegal = (record,message) =>{
  //   console.log(record)
  // }

  // const showAssignVirtualBank = (record) =>{
  //   dispatch({
  //     type: "users/showAssignVirtualAccountForm",
  //     payload: record,
  //   })
  // }
  //
  // const closeAssignVirtualBank = (record) =>{
  //   dispatch({
  //     type: "users/closeAssignVirtualAccountForm",
  //     payload: {},
  //   })
  // }

  // const showRejectVirtualBank = (record) =>{
  //   console.log(record)
  // }

  return (
    <div>
      <Row>
        <Col>
          <Card>
            <Table columns={columns} dataSource={users.users} loading={loading.effects['users/fetchUsers']}></Table>
          </Card>
        </Col>
      </Row>
      <Row style={{marginTop:"16px"}} gutter={16}>
        <Col span={12}>
          <Card title="实名认证申请">
            <UserCertifications
              dispatch={dispatch}
              actionCompleted={users.certificationOptCompleted}
              certifications={users.auths?users.auths.content:[]}
              loading={loading.effects['users/fetchUserAuths']}
            />
          </Card>
        </Col>

        <Col span={12}>
          <Card title="银行账户申请">
            <VirtualAccounts
              dispatch={dispatch}
              virtualAccountOperation = { users.virtualAccountOperation }
              entities={users.virtualAccounts?users.virtualAccounts.content:[]}
              loading={loading.effects['users/fetchVirtualAccountApplications']}
            />
          </Card>
        </Col>
      </Row>
    </div>
  )
}

Users.propTypes = {
  dispatch: PropTypes.func,
  loading: PropTypes.object,
}

export default connect(({ loading, users }) => ({ loading, users }))(Users)
