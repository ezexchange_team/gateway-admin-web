import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { Card, Col, Dropdown, Icon, Menu, Row, Tag, Table, Avatar, Modal } from 'antd';
import { config } from 'utils';
import styles from './index.less';
import { DataTable } from 'components';
import UserLegalEntityList from './UserLegalEntityList'
import VirtualAccountList from './VirtualAccountList'
import AssignVirtualAccountForm from '../commons/VirtualAccounts/AssignVirtualAccountForm'

const User = ({ loading, dispatch, user }) => {

  // console.log(user)
  // const { users } = datas;
  const approveUserLegal = (data) =>{
    console.log(data)
  }
  const rejectUserLegal = (data,reson) =>{
    console.log(data);
    console.log(reson);
  }

  const showAssignVirtualBank = (record) =>{
    dispatch({
      type: "user/showAssignVirtualAccountForm",
      payload: record,
    })
  }

  const closeAssignVirtualBank = (record) =>{
    dispatch({
      type: "user/closeAssignVirtualAccountForm",
      payload: {},
    })
  }


  const assignVirtualBank = (record) =>{
    dispatch({
      type: "user/assignVirtualBankAccount",
      payload: record,
    })

  }


  const showRejectVirtualBank = (record) =>{
    console.log(record)
  }

  const rejectVirtualBank = (record) =>{
    console.log(record)
  }

  const fetchAmzFinanceEvent = (record) => {
    dispatch({
      type:'user/fetchUserAmzFinanceEvent'
    })
  }

  // const menu = (
  //   <Menu>
  //     <Menu.Item key="0">
  //       <a href="http://www.alipay.com/">实名通过</a>
  //     </Menu.Item>
  //     <Menu.Divider />
  //     <Menu.Item key="1">
  //       <a href="http://www.taobao.com/">证件照片不清楚</a>
  //     </Menu.Item>
  //     <Menu.Item key="3">无法查询实名信息</Menu.Item>
  //   </Menu>
  // )
  return (
    <div>
      <Row gutter={32}>
        <Col span={12}>
          <Card>
            <Row>
              <Col>
                <div style={{float:'left'}}>
                  <Avatar size="default" icon="user" shape="circle"/>
                </div>
                <div style={{float:'left',marginLeft:'8px'}}>
                  <div className={styles.text}>#{user.userInfo.id}</div>
                  <div className={styles.text}>{user.userInfo.email}</div>
                </div>
                <div style={{float:'right',marginLeft:'8px'}}>
                  $<span style={{fontSize:'28px'}}>{user.userInfo.balance}</span>
                </div>
              </Col>

            </Row>

            <Row>
              <Col>
                <hr style={{marginTop:'8px'}} />
              </Col>
            </Row>
          </Card>

        </Col>

        <Col sm={24} xs={24} md={12} lg={12} xl={8}>

          <Row>
            <Col>
              <Card title="实名信息">
                <UserLegalEntityList
                  entities={user.userLegalEntities}
                  approveOpt={approveUserLegal}
                  rejectOpt={rejectUserLegal}
                />
              </Card>

            </Col>


            <Col style={{ marginTop: '16px' }}>
              <Card className={styles.card} title="收款账号申请" bordered={false}>
                <VirtualAccountList
                  loading = {loading.effects['user/fetchUserAmazonEntities']}
                  approveOpt={showAssignVirtualBank}
                  rejectOpt={showRejectVirtualBank}
                  expandOpt={fetchAmzFinanceEvent}
                  entities={user.userAmazonEntities}
                />
              </Card>
            </Col>

            {/*<Col style={{ marginTop: '16px' }}>*/}
              {/*<Card className={styles.card} title="EBAY店铺">*/}
                {/*<Row>*/}
                  {/*<Col span={8}>*/}
                    {/*<dl className="">*/}
                      {/*<dt><strong>实名</strong></dt>*/}
                      {/*<dd>邓育铭</dd>*/}
                      {/*<dt><strong>证件号码</strong></dt>*/}
                      {/*<dd>440902199512280136</dd>*/}
                    {/*</dl>*/}
                    {/*<Tag color="green">实名验证通过</Tag>*/}
                  {/*</Col>*/}
                  {/*<Col span={16}>*/}
                    {/*<Row type="flex" justify="start">*/}
                      {/*<Col span={12}><Card bodyStyle={{ padding: 0 }}>*/}
                        {/*<div className="custom-image">*/}
                          {/*<img alt="example" width="100%" src="https://id.transfer-express.net/Fq-ldc0Axb2kGUpeh6RupTxFB7NV"/>*/}
                        {/*</div>*/}

                      {/*</Card></Col>*/}
                      {/*<Col span={12}><Card bodyStyle={{ padding: 0 }}>*/}
                        {/*<div className="custom-image">*/}
                          {/*<img alt="example" width="100%" src="https://id.transfer-express.net/Fpx2C7jjt3VxnupXc2aNSACS58uS"/>*/}
                        {/*</div>*/}
                      {/*</Card></Col>*/}
                    {/*</Row>*/}
                  {/*</Col>*/}
                {/*</Row>*/}
              {/*</Card>*/}
            {/*</Col>*/}
          </Row>
        </Col>
      </Row>

      <AssignVirtualAccountForm
        loading = {user.assignBankAccountLoading}
        data = {user.assignBankAccountData}
        onSubmit={assignVirtualBank}
        onClose={closeAssignVirtualBank}
        visible={user.assignBankAccountVisible}
      />
    </div>
  )
}

User.propTypes = {
  dispatch: PropTypes.func,
  loading: PropTypes.object,
  user: PropTypes.object,
}

export default connect(({ loading, user }) => ({ loading, user }))(User)
