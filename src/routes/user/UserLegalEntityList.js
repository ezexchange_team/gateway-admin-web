import React from 'react'
import { Button, Dropdown, Icon, Menu, Table, Badge } from 'antd';

const UserLegalEntityList = ({ loading, entities, approveOpt, rejectOpt }) => {
  let selected = null;
  const handleMenuClick = (item) => {
    // console.log(item)
    // console.log(item.key)
    // if()
    // console.log(selected)
    switch (item.key) {
      case 0:
        rejectOpt(selected, '实名信息不完整')
        break
      case 1:
        rejectOpt(selected, '证件扫描资料无法识别')
        break
      case 2:
        rejectOpt(selected, '无法核实实名信息')
        break
      default:
        rejectOpt(selected, '无法核实实名信息')
        break
    }


  }
  const select = (item) => {
    selected = item
  }
  const menu = (
    <Menu onClick={handleMenuClick}>
      <Menu.Item key="0">实名信息不完整</Menu.Item>
      <Menu.Item key="1">证件扫描资料无法识别</Menu.Item>
      <Menu.Item key="2">无法核实实名信息</Menu.Item>
    </Menu>
  )

  const columns = [{
    title: '#',
    dataIndex: 'id',
  }, {
    title: '实名',
    dataIndex: 'legalEntity.entityName',
  }, {
    title: '证件号码',
    dataIndex: 'legalEntity.identityNumber',
  }, {
    title: "状态",
    dataIndex: "legalEntity.status",
    render: (text) =>{
      if(text === 0) {
        return <Badge status="processing" text="待审核" />
      } else if(text < 0){
        return <Badge status="error" text="验证失败" />
      } else {
        return <Badge status="success" text="审核通过" />
      }
    }
  }, {
    title: '操作',
    dataIndex: "legalEntity.status",
    key:"operation",
    render: (text,record) => {
      if(text === 0){
        return (
          <div>
            <a className="ant-dropdown-link">通过</a>
            <Dropdown overlay={menu} trigger={['click']}>
              <a className="ant-dropdown-link" href="#" style={{marginLeft:"4px",paddingLeft:"4px",borderLeft:"1px solid"}}>
                拒绝 <Icon type="down" />
              </a>
            </Dropdown>
          </div>

        )
      } else {
        return <div>
          <a className="ant-dropdown-link" disabled={true}>通过</a>
          <Dropdown overlay={menu} trigger={['click']} disabled={true} >
            <a  className="ant-dropdown-link" href="#" style={{marginLeft:"4px",paddingLeft:"4px",borderLeft:"1px solid"}}>
              拒绝 <Icon type="down" />
            </a>
          </Dropdown>
        </div>
      }
    },
  }]

  const expendRowRender = (record) => {
    let identityFiles = [];
    if (record.legalEntity.identityFiles) {
      identityFiles = record.legalEntity.identityFiles.split(',')
    }
    return (
      <dl className="dl-horizontal">
        {
          identityFiles.map((item, index) => <div key={index}>
            <dt>认证文件</dt>
            <dd><a href={item} target="about:blank">{item}</a></dd>
          </div>)
        }


      </dl>
    )
  }
  // console.log(entities)

  // return (
  //   <div>
  //
  //     {
  //
  //       entities.map((item) => {
  //         return (<div>
  //           <Row style={{ borderBottom: '1px solid #ececec', paddingBottom: '16px', paddingTop: '16px' }}>
  //             <Col span={8}>
  //               <dl className="">
  //                 <dt><strong>实名</strong></dt>
  //                 <dd>{item.legalEntity.entityName}</dd>
  //                 <dt><strong>证件号码</strong></dt>
  //                 <dd>{item.legalEntity.identityNumber}</dd>
  //               </dl>
  //
  //               {
  //                 (item.legalEntity.status === 0 && (<Tag color="red">dfdfd</Tag>)) ||
  //                 (item.legalEntity.status === 1 && (<Tag color="green" >通过实名验证</Tag>)) ||
  //                 (item.legalEntity.status === -1 && (<Tag color="default">未通过验证</Tag>))
  //
  //
  //
  //               }
  //
  //             </Col>
  //             <Col span={16}>
  //               {item.legalEntity.identityFiles && item.legalEntity.identityFiles.split(',').map(_item => (
  //                 <Row type="flex" justify="start">
  //                   <Col span={12}><Card bodyStyle={{ padding: 0 }}>
  //                     <div className="custom-image">
  //                       <img alt="example" width="100%" src={_item}/>
  //                     </div>
  //
  //                   </Card></Col>
  //
  //                 </Row>
  //               ))}
  //             </Col>
  //           </Row>
  //
  //         </div>)
  //       })
  //     }
  //   </div>
  // )

  return (
    <Table
      columns={columns}
      dataSource={entities}
      expandedRowRender={expendRowRender}
      rowKey={record => record.id}
    ></Table>
  )

}

export default UserLegalEntityList
