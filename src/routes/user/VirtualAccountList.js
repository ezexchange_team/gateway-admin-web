import React from 'react'
import { Badge, Dropdown, Icon, Menu, Table, message } from 'antd';

const VirtualAccountList = ({ loading, entities, approveOpt, rejectOpt, expandOpt }) => {
  console.log(entities)
  const handleMenuClick = (record,item) => {
    console.log(record)
    switch (item.key) {
      case '0':
        approveOpt(record);
        break;
      case '1':
        rejectOpt(record);
        break;
      case '2':
        expandOpt(record);
        break;
      default:
        message.error("错误操作");
        break;
    }
  }

  const columns = [{
    title: '账户名',
    dataIndex: 'bankAccountName',
  }, {
    title: '账户号',
    dataIndex: 'bankAccountNumber',
  }, {
    title: '路由号',
    dataIndex: 'routingNumber',
  }, {
    title: '状态',
    dataIndex: 'amazonLegalEntity.status',
    render: (text, record) => {
      if (text === 0) {
        return <Badge status="processing" text="待审核"/>
      } else if (text < 0) {
        return <Badge status="error" text="已拒绝"/>
      } else if (text > 0) {
        return <Badge status="success" text="已通过"/>
      }
    }
  }, {
    title: '操作',
    dataIndex: 'amazonLegalEntity.status',
    key: 'operation',
    render: (text, record) => {
      if (text === 0) {
        return (
          <div>
            <Dropdown overlay={
              <Menu onClick={handleMenuClick.bind(this, record)}>
                <Menu.Item key="0"><Icon type="bank"></Icon> 分配账号</Menu.Item>
                <Menu.Item key="1"><Icon type="close"></Icon> 拒绝开户</Menu.Item>
                <Menu.Item key="2"><Icon type="search"></Icon> 查看店铺3个月流水</Menu.Item>
              </Menu>
            } trigger={['click']}>
              <a className="ant-dropdown-link" href="#">
                ... <Icon type="down"/>
              </a>
            </Dropdown>
          </div>

        )
      } else {
        return <div>
          <Dropdown overlay={
            <Menu onClick={handleMenuClick}>
              <Menu.Item key="0" disabled={true}><Icon type="bank"></Icon> 分配账号</Menu.Item>
              <Menu.Item key="1" disabled={true}><Icon type="close"></Icon> 拒绝开户</Menu.Item>
              <Menu.Item key="2"><Icon type="search"></Icon> 查看店铺3个月流水</Menu.Item>
            </Menu>
          } trigger={['click']}>
            <a className="ant-dropdown-link" href="#">
              ... <Icon type="down"/>
            </a>
          </Dropdown>
        </div>
      }
    },
  }]


  return (
    <Table
      loading={loading}
      columns={columns} dataSource={entities} rowKey={record => record.amazonLegalEntity.id}>

    </Table>
  )

}

export default VirtualAccountList
