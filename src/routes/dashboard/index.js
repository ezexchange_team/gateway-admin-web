import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'dva'
import { Button, Row, Col, Table, Card } from 'antd'
import { config } from 'utils'
import styles from './index.less'
import QueryString from 'query-string'
import { DataTable } from 'components'

const Dashboard = ({ loading, dispatch, dashboard}) => {

  console.log(dashboard)
  return (
    <div>
      <Row gutter={32}>
        <Col span={16}>
          <Card bordered={false}>
            <DataTable/>
          </Card>

        </Col>
      </Row>
    </div>
  )
}

Dashboard.propTypes = {
  dispatch: PropTypes.func,
  loading: PropTypes.object,
  dashboard: PropTypes.object,
}

export default connect(({ loading, dashboard }) => ({ loading, dashboard }))(Dashboard)
