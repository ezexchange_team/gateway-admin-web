import React from 'react'
import { Badge, Dropdown, Form, Icon, Input, Menu, message, Modal, Select, Table } from 'antd';
import AssignVirtualAccountForm from './AssignVirtualAccountForm'
import RejectVirtualAccountForm from './RejectVirtualAccountForm'
import AmazonStoreFinancialEvents from './AmazonStoreFinancialEvents'

const VirtualAccounts = ({
                                      loading,
                                      dispatch,
                                      entities,
                                      virtualAccountOperation
                                    }) => {


  const handleMenuClick = (record, item) => {
    switch (item.key) {
      case '0':
        openOperationModal(0, record);
        break;
      case '1':
        openOperationModal(1, record);
        break;
      case '2':
        openOperationModal(2, record);
        break;
      default:
        message.error('错误操作');
        break;
    }
  }

  const openOperationModal = (type, record) => {
    dispatch({
      type: 'users/openOperationModal',
      payload: { type, record },
    })
  }

  const closeOperationModal = (type, record) => {
    dispatch({
      type: 'users/closeOperationModal',
      payload: { type, record },
    })
  }

  const assignVirtualBank = (record) => {
    dispatch({
      type: 'users/assignVirtualBankAccount',
      payload: record,
    })
  }

  const rejectVirtualBank = (record) =>{
    dispatch({
      type: 'users/rejectVirtualBankAccount',
      payload: record,
    })
  }

  const columns = [{
    title: '#',
    dataIndex: 'id',
  }, {
    title: '用户ID',
    dataIndex: 'userId',
  }, {
    title: '状态',
    dataIndex: 'legalEntity.status',
    render: (text, record) => {
      if (text === 0) {
        return <Badge status="processing" text="待审核"/>
      } else if (text < 0) {
        return <Badge status="error" text="已拒绝"/>
      } else if (text > 0) {
        return <Badge status="success" text="已通过"/>
      }
    }
  }, {
    title: '操作',
    dataIndex: 'legalEntity.status',
    key: 'operation',
    render: (text, record) => {
      if (text === 0) {
        return (
          <div>
            <Dropdown overlay={
              <Menu onClick={handleMenuClick.bind(this, record)}>
                <Menu.Item key="0"><Icon type="bank"></Icon> 分配账号</Menu.Item>
                <Menu.Item key="1"><Icon type="close"></Icon> 拒绝开户</Menu.Item>
                <Menu.Item key="2"><Icon type="search"></Icon> 查看店铺3个月流水</Menu.Item>
              </Menu>
            } trigger={['click']}>
              <a className="ant-dropdown-link" href="#">
                ... <Icon type="down"/>
              </a>
            </Dropdown>
          </div>

        )
      } else {
        return <div>
          <Dropdown overlay={
            <Menu onClick={handleMenuClick}>
              <Menu.Item key="0" disabled={true}><Icon type="bank"></Icon> 分配账号</Menu.Item>
              <Menu.Item key="1" disabled={true}><Icon type="close"></Icon> 拒绝开户</Menu.Item>
              <Menu.Item key="2"><Icon type="search"></Icon> 查看店铺3个月流水</Menu.Item>
            </Menu>
          } trigger={['click']}>
            <a className="ant-dropdown-link" href="#">
              ... <Icon type="down"/>
            </a>
          </Dropdown>
        </div>
      }
    },
  }]

  const expandedRowRender = (record) => {
    return (
      <div>
        <div>Seller ID: {record.legalEntity.sellerId}</div>
        <div>Access ID: {record.legalEntity.accessId}</div>
        <div>Secrret Key: {record.legalEntity.secretKey}</div>
      </div>
    )
  }

  // console.log(visibleOpterationType)
  console.log(virtualAccountOperation)
  return (
    <div>
      <Table
        loading={loading}
        columns={columns} dataSource={entities} rowKey={record => record.id} expandedRowRender={expandedRowRender}>

      </Table>

      <AssignVirtualAccountForm
        loading={virtualAccountOperation.loading}
        data={virtualAccountOperation.virtualBankAccount}
        onSubmit={assignVirtualBank}
        onClose={closeOperationModal}
        visible={(virtualAccountOperation.visibleOpterationType === 0)}
      />

      <RejectVirtualAccountForm
        loading={virtualAccountOperation.loading}
        data={virtualAccountOperation.virtualBankAccount}
        onSubmit={rejectVirtualBank}
        onClose={closeOperationModal}
        visible={(virtualAccountOperation.visibleOpterationType === 1)}
      />

      <AmazonStoreFinancialEvents
        loading={virtualAccountOperation.loading}
        data={virtualAccountOperation.extraData}
        onClose={closeOperationModal}
        visible={(virtualAccountOperation.visibleOpterationType === 2)}
      />
    </div>

  )

}



export default VirtualAccounts
