import React from 'react'
import { Form, Input, Modal, Select } from 'antd';

const RejectVirtualAccountForm = (
  { loading,
    onSubmit,
    onClose,
    form: { getFieldDecorator, validateFieldsAndScroll, resetFields, }, visible, data
  }) => {

    const handleSubmit = () => {
    validateFieldsAndScroll((errors, values) => {
      if (errors) {
        return
      }
      onSubmit(Object.assign({}, values, { id: data.legalEntity.id }));
      resetFields();

    })
  }

  const handleClose = () => {
    resetFields();
    onClose();
  }


  const formLayout = {
    wrapperCol: { span: 14 },
    labelCol: { span: 6 }
  }

  return (
    <Modal visible={visible} title="拒绝开户" onOk={handleSubmit} onCancel={handleClose} confirmLoading={loading}>
      <Form layout="horizontal">
        <Form.Item
          {...formLayout}
          label="账户名"
        >
          {
            getFieldDecorator('accountName', {
                rules: [
                  { required: true, message: '账户名不能为空' }
                ],
                initialValue: data.accountHolderName,
              },
            )(
              <Input disabled={true}/>
            )
          }
        </Form.Item>

        <Form.Item
          {...formLayout}
          label="拒绝原因"
        >
          {
            getFieldDecorator('reson', {
              rules: [
                { required: true, message: '拒绝原因不能为空' }
              ]
            })(
              <Select>
                <Select.Option value="店铺信息无法核实" key="254070116">店铺信息无法核实</Select.Option>
                <Select.Option value="系统审核未通过" key="122203950">系统审核未通过</Select.Option>
              </Select>
            )
          }
        </Form.Item>
      </Form>
    </Modal>
  )

}

export default Form.create()(RejectVirtualAccountForm);
