import React from 'react'
import { Modal, Table, Badge } from 'antd';

const AmazonStoreFinancialEvents = (
  { loading,
    onClose,
    visible,
    data,
  }) => {


  const handleClose = () => {
    onClose();
  }
  console.log(data);

  const columns = [
    {title: "放款时间",dataIndex: "fundTransferDate"},
    {title: "金额",dataIndex: "originalTotal.currencyAmount"},
    {title: "处理状态",dataIndex: "fundTransferStatus",render: (text,record) =>{
      if(text === null){
        return <Badge status="processing" text="进行中"/>
      } else if(text === 'Succeeded') {
        return <Badge status="success" text="放款完成"/>
      } else if( text === 'Failed') {
        return <Badge status="error" text="放款失败"/>
      } else{
        return <Badge status="default" text="位置状态"/>
      }
    }},
    {title: "账户",dataIndex: "accountTail"},
    {title: "货币",dataIndex: "originalTotal.currencyCode"},
  ]

  return (
    <Modal visible={visible} title="分配账号" onCancel={handleClose}>
      <Table columns={columns} dataSource={data?data.financialEventGroupList:[]} pagination={{defaultPageSize:5}}></Table>
    </Modal>
  )

}

export default AmazonStoreFinancialEvents;
