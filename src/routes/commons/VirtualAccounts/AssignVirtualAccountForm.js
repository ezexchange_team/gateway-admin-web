import React from 'react'
import { Form, Input, Modal, Select } from 'antd';

const AssignVirtualAccountForm = (
  { loading,
    onSubmit,
    onClose,
    form: { getFieldDecorator, validateFieldsAndScroll, resetFields, }, visible, data
  }) => {

  const handleSubmit = () => {
    validateFieldsAndScroll((errors, values) => {
      if (errors) {
        return
      }
      onSubmit(Object.assign({}, values, { id: data.id }));
      resetFields();

    })
  }

  const handleClose = () => {
    resetFields();
    onClose();
  }


  const formLayout = {
    wrapperCol: { span: 14 },
    labelCol: { span: 6 }
  }
  return (
    <Modal visible={visible} title="分配账号" onOk={handleSubmit} onCancel={handleClose} confirmLoading={loading}>
      <Form layout="horizontal">
        <Form.Item
          {...formLayout}
          label="账户名"
        >
          {
            getFieldDecorator('accountName', {
                rules: [
                  { required: true, message: '账户名不能为空' }
                ],
                initialValue: data.accountHolderName,
              },
            )(
              <Input disabled={true}/>
            )
          }
        </Form.Item>

        <Form.Item
          {...formLayout}
          label="路由号"
        >
          {
            getFieldDecorator('routingNumber', {
              rules: [
                { required: true, message: '路由号不能为空' }
              ]
            })(
              <Select>
                <Select.Option value="254070116" key="254070116">花旗 - 254070116</Select.Option>
                <Select.Option value="122203950" key="122203950">国泰 - 122203950</Select.Option>
                <Select.Option value="031000503" key="031000503">富国 - 031000503</Select.Option>
              </Select>
            )
          }
        </Form.Item>

        <Form.Item
          {...formLayout}
          label="账户号"
        >
          {
            getFieldDecorator('accountNumber', {
              rules: [
                { required: true, message: '账户号不能为空' }
              ]
            })(
              <Input/>
            )
          }
        </Form.Item>


      </Form>
    </Modal>
  )

}

export default Form.create()(AssignVirtualAccountForm);
