import { request, config } from 'utils'
import QuryString from 'query-string'
const { api } = config
const { userAuths, passUserCertification, rejectCertification } = api
/**
 * 获取实名认证列表
 * @returns {Promise.<*>}
 */
export async function fetchUserAuths (params) {
  const auth = sessionStorage.getItem("auth");
  return request({
    url: userAuths,
    method: 'get',
    headers: {
      Authorization: auth,
      "Content-Type": 'application/json',
    },
    params,
  })
}


export async function  approveUserCertifiction({entityId}) {
  const auth = sessionStorage.getItem("auth")
  return request({
    url: passUserCertification.replace(':entityId', entityId),
    method: 'put',
    headers: {
      'Content-Type': 'application/json',
      Authorization: auth,
    },
  })
}

export async function  rejectUserCertifiction({entityId, reson}) {
  const auth = sessionStorage.getItem("auth")
  return request({
    url: rejectCertification.replace(':entityId', entityId),
    method: 'put',
    headers: {
      'Content-Type': 'application/json',
      Authorization: auth,
    },
    data: {reson}
  })
}




