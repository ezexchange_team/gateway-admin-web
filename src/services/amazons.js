import { request, config } from 'utils'

const { api } = config
const { amazonFinancialEvents } = api

export async function fetchAmazonFinancialEvents (params) {
  console.log(params)
  const auth = sessionStorage.getItem("auth")
  const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
  return request({
    url: amazonFinancialEvents,
    method: 'get',
    headers: {
      "Authorization": auth,
    },
    params,
  })
}


