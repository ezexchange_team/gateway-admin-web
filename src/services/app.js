import { request, config } from 'utils'

const { api } = config
const { user, userLogout, userLogin } = api

export async function login (params) {
  return request({
    url: userLogin,
    method: 'post',
    data: params,
  })
}

export async function logout (params) {
  return request({
    url: userLogout,
    method: 'get',
    data: params,
  })
}

export async function query (params) {
  const auth = sessionStorage.getItem("auth")
  const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
  return request({
    url: user.replace(':id', userInfo.id),
    method: 'get',
    headers:{
      "Authorization":auth
    },
    data: params,
  })
}
