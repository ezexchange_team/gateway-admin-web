import { request, config } from 'utils'
import QuryString from 'query-string'
const { api } = config
const { userInfo, users,virtualAccountApplicatijons } = api

export async function fetchGatewayUserInfo () {
  const auth = sessionStorage.getItem("auth");
  return request({
    url: userInfo.replace(':id', 164),
    method: 'get',
    headers: {
      Authorization: auth,
      "Content-Type": 'application/json',
    },
  })
}

export async function fetchUsers (params) {
  const auth = sessionStorage.getItem("auth")
  const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
  return request({
    url: users,
    method: 'get',
    headers: {
      Authorization: auth,
    },
  })
}

export async function fetchVirtualAccountApplications (params) {
  const auth = sessionStorage.getItem("auth")
  const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
  return request({
    url: virtualAccountApplicatijons,
    method: 'get',
    headers: {
      Authorization: auth,
    },
    params,
  })
}




