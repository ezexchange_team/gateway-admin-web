import { request, config } from 'utils'

const { api } = config
const { assignVirtualBank, rejectVirtualBank } = api

export async function assignVirtualBankAccount (data) {
  const auth = sessionStorage.getItem("auth")
  const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
  return request({
    url: assignVirtualBank.replace(':id',data.id),
    method: 'put',
    headers: {
      "Authorization": auth,
    },
    data,
  })
}

export async function rejectVirtualBankAccount(data) {
  const auth = sessionStorage.getItem("auth")
  const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
  return request({
    url: rejectVirtualBank.replace(':id',data.id),
    method: 'put',
    headers: {
      "Authorization": auth,
    },
    data,
  })
}

