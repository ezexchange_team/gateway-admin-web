import { request, config } from 'utils'

const { api } = config
const { user, userLegalEntities, userAmazonEntities, passUserCertification } = api


export async function fetchUserDetail (params) {
  const auth = sessionStorage.getItem("auth")
  const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
  return request({
    url: user.replace(':id',params.id),
    method: 'get',
    headers: {
      Authorization: auth,
    },
  })
}

export async function fetchUserLegalEntities ({userId}) {
  const auth = sessionStorage.getItem("auth")
  console.log(auth);

  return request({
    url: userLegalEntities.replace(':id', userId),
    method: 'get',
    headers: {
      Authorization: auth,
    },
  })
}

export async function fetchUserAmazonEntities ({userId}) {
  const auth = sessionStorage.getItem("auth")
  return request({
    url: userAmazonEntities.replace(':id', userId),
    method: 'get',
    headers: {
      'Content-Type': 'application/json',
      Authorization: auth,
    },
  })
}


